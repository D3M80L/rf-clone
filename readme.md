Decode and send RF433 signals using a cheap RF433 modules. There are many libraries avaialable, which allow to send and receive RF433 signals - one of them is the [rc-switch](https://github.com/sui77/rc-switch) library. Unfortunatelly, it seems that my devices do not cooperate with this library and I do not know how to use them with my devices, so I decided to create my simple project. The challenge was, how to read and send those signals, after reading few sites I found [very nice tutorial](http://arduinobasics.blogspot.com.au/2014/06/433-mhz-rf-module-with-arduino-tutorial.html) which helped me to understand, decode and sucessfully send the signals. This project is an extension of the above tutorial.

# Read RF433 signal

![Arduingo Board connection](reader-arduino.png)

* Use 3,3 voltage to decrease the receiving area of the module. To learn or receive the signal from a transmitter (like MacLean MCE07 remote control, remote RGB controllers or other devices)

* Upload the rc-clone-reader.ino project to Arduino Uno
* Press a single key on the remote controller (remember to put the device at very close distance to the receiver)
* Copy the MAX,MIN values to CODE_ANALYSE tab from signal_analysis sheet.
* Manually change the signal to group spreaded values and clean the code
    * For example, having 23 reads of 4 in the HI column, and 52 reads of 5 in HI column, then rename all 4s to 5s in the HI column. As a result you will have 75 reads of 5 in HI column.
* Define code map, by collecting keys in the yellow area in the CODE_ANALYSE tab
* Now copy the CODE column and find a common pattern in the signal, as a reference use chart, which will display similarities
* Define the code signature in Arduino project or in .NET Core project - and use the sender to send the signal

# Send RF433 signal
![Arduino Board connection](sender-arduino.png)
* Attach small antenna to the sender module
* Upload the Arduino sender program to Arduino
## Sending signals
There are fewa ways to send the signal - please look at the examples for detailed reference.

* You can use the SignalSender object in Arduino, which takes the digital pin number
```cpp
SignalSender signalSender(4);
```

* You can use the .NET Core project to send previously generated signal values to Arduino via USB 

```csharp
using (var signalSignatureSender = new SignalSignatureSender())
{
    signalSignatureSender.Open(portName);

    signalSignatureSender.Send(new SwitchOneOn());
}
```

# Likns
* [rc-switch](https://github.com/sui77/rc-switch) - nice library which supports many RC devices (unfortunatelly mine, also I was not able to run my devices on it) 
* [433 MHz RF module with Arduino Tutorials](http://arduinobasics.blogspot.com.au/2014/06/433-mhz-rf-module-with-arduino-tutorial.html) - a very nice tutorial with detailed expanation - the reader module and reading method available which I used in this repo are based on this tutorial.
* [Reverse Engineer Wireless Temperature / Humidity / Rain Sensors](http://rayshobby.net/reverse-engineer-wireless-temperature-humidity-rain-sensors-part-1/) - nice to read 
* [Decoding and Sending 433MHz RF Codes With Arduino and Rc-switch](http://www.instructables.com/id/Decoding-and-sending-433MHz-RF-codes-with-Arduino-/)
* [serialapp](https://github.com/Ellerbach/serialapp) - a .NET Core 2 library for Serial communication on linux
#ifndef ProcessingSignatureCommand_h
  #define ProcessingSignatureCommand_h

  #include <stdint.h>
  #include <stdbool.h>
  #include <Arduino.h>

  #include "SignalSender.h"

  class ProcessSignatureCommand {
    public:
      ProcessSignatureCommand(const SignalSender& signalSender);
      bool Process(byte incomingByte);
    private:
      void rememberByte(byte incomingByte);
      const SignalSender& _signalSender;
      enum CommandReadState : byte;
      CommandReadState _currentState;
      int _signatureBufferOffset;
      byte _signatureBuffer[3 + 256 * 2 + 256];
      int _missingSignatureBytes;
  };
  
#endif

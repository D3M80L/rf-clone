#include <stdint.h>
#include <stdbool.h>
#include<stdio.h>
#include<stdlib.h>

#include "ProcessingSignatureCommand.h"
#include "SignalSender.h"

enum ProcessSignatureCommand::CommandReadState : byte {
  AwaitingForStart,
  AwaitingForMapLength,
  ReadingMap,
  AwaitingForDuration,
  AwaitingForSignalLength,
  ReadingSignal
};

ProcessSignatureCommand::ProcessSignatureCommand(const SignalSender& signalSender) :
  _signalSender(signalSender)
{
  _currentState = ProcessSignatureCommand::CommandReadState::AwaitingForStart;
}

bool ProcessSignatureCommand::Process(byte incomingByte) 
{
  if (_currentState == ProcessSignatureCommand::CommandReadState::AwaitingForStart) 
  {
    if (incomingByte != 83) {
      return false;
    }
    _currentState = ProcessSignatureCommand::CommandReadState::AwaitingForMapLength;
    _signatureBufferOffset = 0;
  } 
  else if (_currentState == ProcessSignatureCommand::CommandReadState::AwaitingForMapLength) 
  {
    rememberByte(incomingByte);
    _missingSignatureBytes = incomingByte * 2;
    _currentState = ProcessSignatureCommand::CommandReadState::ReadingMap;
  } 
  else if (_currentState == ProcessSignatureCommand::CommandReadState::ReadingMap) 
  {
    rememberByte(incomingByte);
    --_missingSignatureBytes;
    if (_missingSignatureBytes == 0) {
      _currentState = ProcessSignatureCommand::CommandReadState::AwaitingForDuration;
    }
  } 
  else if (_currentState == ProcessSignatureCommand::CommandReadState::AwaitingForDuration) 
  {
    rememberByte(incomingByte);
    _currentState = ProcessSignatureCommand::CommandReadState::AwaitingForSignalLength;
  } 
  else if (_currentState == ProcessSignatureCommand::CommandReadState::AwaitingForSignalLength) 
  {
    rememberByte(incomingByte);
    _missingSignatureBytes = incomingByte;
    _currentState = ProcessSignatureCommand::CommandReadState::ReadingSignal;
  } 
  else if (_currentState == ProcessSignatureCommand::CommandReadState::ReadingSignal) 
  {
    if (incomingByte > _signatureBuffer[0]) {
      _currentState = ProcessSignatureCommand::CommandReadState::AwaitingForStart;
      return false;
    }
    rememberByte(incomingByte);
    --_missingSignatureBytes;
    if (_missingSignatureBytes == 0) {
      _currentState = ProcessSignatureCommand::CommandReadState::AwaitingForStart;
      
      _signalSender.Send(_signatureBuffer);
    }
  } 
}

void ProcessSignatureCommand::rememberByte(byte incomingByte) {
  _signatureBuffer[_signatureBufferOffset++] = incomingByte;
}


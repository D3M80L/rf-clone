#ifndef SignalSender_h
#define SignalSender_h

#include <Arduino.h>

class SignalSender {
  public:
    SignalSender(int pinNumber);
    void Send(byte *codeMap, byte signalDuration, int signalLength, byte *signalDefinition) const;
    void Send(byte* signature) const;
  private:
    int _pinNumber;
};

#endif

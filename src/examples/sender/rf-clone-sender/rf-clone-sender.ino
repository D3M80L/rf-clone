#include <stdint.h>

#include "knownSignals.h"
#include "ProcessingSignatureCommand.h"
#include "SignalSender.h"

SignalSender signalSender(4);
ProcessSignatureCommand processSignatureCommand(signalSender);

void setup()
{
  Serial.begin(9600);
  Serial.println("Initializing.");

  processSignatureCommand.Process(rgbControl);
}

void loop()
{
  if (Serial.available() > 0)
  {
    int incomingByte = Serial.read();
    processSignatureCommand.Process((byte)incomingByte);
  }
}

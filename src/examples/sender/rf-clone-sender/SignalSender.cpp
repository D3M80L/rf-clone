#include "SignalSender.h"
#include <Arduino.h>

SignalSender::SignalSender(int pinNumber)
{
  _pinNumber = pinNumber;
  pinMode(pinNumber, OUTPUT);
  digitalWrite(pinNumber, LOW);
}

void SignalSender::Send( byte *codeMap, byte signalDuration, int signalLength, byte *signalDefinition) const
{
  for (int r = 0; r < 3; ++r)
  {
    for (int i = 0; i < signalLength; ++i)
    {
      int codePair = signalDefinition[i] * 2;
      int highSignal = codeMap[codePair];
      int lowSignal = codeMap[codePair + 1];

      if (highSignal > 0)
      {
        digitalWrite(_pinNumber, HIGH);
        delayMicroseconds(highSignal * signalDuration);
      }

      digitalWrite(_pinNumber, LOW);
      delayMicroseconds(lowSignal * signalDuration);
    }
  }
}

void SignalSender::Send(byte* signature) const
{
  int mapLength = signature[0];
  byte* codeMap = &signature[1];

  int signalDuration = signature[1 + mapLength * 2];
  int signalLength = signature[2 + mapLength * 2];
  
  byte* signalDefinition = &signature[3 + mapLength * 2];

  Send (codeMap, signalDuration, signalLength, signalDefinition);
}


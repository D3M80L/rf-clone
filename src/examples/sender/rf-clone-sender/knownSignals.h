#ifndef knownSignals_h
  #define knownSignals_h

  byte macLeanSwitch[] = {
    4, // CODE MAP LENGTH
    5, 5,
    5, 12,
    30, 30,
    5, 90,

    105, // SIGNAL DURATION
    44, // SIGNAL LENGTH

     0,0,2,0,0,0,0,0,1,1,1,1,1,0,1,0,1,1,0,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,1,1,0,0,0,0,1,1,1,3 // BTN1_ON
    //   0,0,2,0,0,0,0,0,1,1,1,1,1,0,1,0,1,1,0,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,1,0,1,1,1,1,0,0,0,3 // BTN1_OFF

    // 0,0,2,0,0,0,0,0,1,1,1,1,1,0,1,0,1,1,0,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,1,0,1,0,0,1,0,1,1,3 // BTN2_ON
    //0,0,2,0,0,0,0,0,1,1,1,1,1,0,1,0,1,1,0,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,1,1,0,1,1,0,1,0,0,3 // BTN2_OFF
  };


  byte rgbControl[] = {
    7, // CODE MAP LENGTH
    2, 2,
    11, 11,
    2, 5,
    5, 2,
    5, 255,
    0, 255,
    0, 46,

    105, // SIGNAL DURATION
    72, // SIGNAL LENGTH

    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,A,A,A,B,B,A,A,A,B,B,A,A,A,A,B,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,A,A,A,B,B,A,A,A,B,B,A,A,A,A,4,5,6 // TEMPLATE

    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,2,2,3,2,2,3,2,3,3,3,2,2,3,3,3,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,3,2,2,3,2,2,3,2,3,3,3,2,2,3,3,4,5,6 // WHITE
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,3,2,2,3,2,2,3,3,3,3,3,2,3,2,2,2,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,3,2,2,3,2,2,3,3,3,3,3,2,3,2,2,4,5,6 // COLORDN
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,3,2,2,3,2,2,2,3,3,3,3,2,2,3,3,2,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,3,2,2,3,2,2,2,3,3,3,3,2,2,3,3,4,5,6 // LIGHTDN
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,3,2,2,3,2,2,2,2,3,3,3,2,2,3,2,3,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,3,2,2,3,2,2,2,2,3,3,3,2,2,3,2,4,5,6 // LIGHTUP

    0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,2,3,3,3,3,3,2,3,2,2,3,2,2,3,3,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,2,3,3,3,3,3,2,3,2,2,3,2,2,3,4,5,6 // GREEN
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,2,2,3,3,3,2,2,2,3,3,3,2,2,2,3,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,2,2,3,3,3,2,2,2,3,3,3,2,2,2,4,5,6 // BLUE
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,3,3,2,2,2,3,3,3,2,3,3,3,2,2,2,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,3,3,2,2,2,3,3,3,2,3,3,3,2,2,4,5,6 // RED
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,3,3,3,2,3,3,2,3,3,2,2,3,2,3,3,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,3,3,3,2,3,3,2,3,3,2,2,3,2,3,4,5,6 // PURPLE
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,2,2,2,3,3,2,2,2,3,3,2,2,2,2,3,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,2,2,2,3,3,2,2,2,3,3,2,2,2,2,4,5,6 // NAVY BLUE
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,3,2,2,3,3,2,2,3,2,3,2,2,2,2,3,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,3,2,2,3,3,2,2,3,2,3,2,2,2,2,4,5,6 // YELLGREEN
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,2,3,2,3,2,2,3,2,3,3,3,3,3,3,2,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,2,3,2,3,2,2,3,2,3,3,3,3,3,3,4,5,6 // LIGHT BLUE
    // 0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,3,2,3,2,3,2,2,3,2,3,2,3,2,2,3,0,0,1,2,2,3,2,3,2,3,2,3,2,2,2,2,2,2,2,2,3,2,3,2,3,2,2,3,2,3,2,3,2,2,4,5,6 // YELLOW
  };
  
#endif

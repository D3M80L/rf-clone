﻿using SignalSender.Signals.MacLean;
using SignalSenter.Usb;
using System;
using System.IO.Ports;
using System.Threading.Tasks;

namespace SenderClient
{
    public class Program
    {
        static void Main(string[] args)
        {
            foreach (var portName in SerialPort.GetPortNames())
            {
                Console.WriteLine(portName);

                using (var signalSignatureSender = new UsbSignalSignatureSender())
                {
                    signalSignatureSender.Open(portName);

                    signalSignatureSender.Send(new SwitchOneOn());

                    Task.Delay(1500).Wait();

                    signalSignatureSender.Send(new SwitchOneOff());
                }
            }
        }
    }
}

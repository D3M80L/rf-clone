﻿using SignalSender;
using System;
using System.IO.Ports;

namespace SignalSenter.Usb
{
    public sealed class UsbSignalSignatureSender : ISignalSignatureSender
    {
        private const byte _sendCommand = 83;

        private SerialPort _serialPort;

        private byte[] _signalSignatureBuffer = new byte[1 + 1 + 256 * 2 + 1 + 1 + 256];

        public void Open(string portName)
        {
            var port = new SerialPort(portName)
            {
                BaudRate = 9600,
                Parity = Parity.None,
                StopBits = StopBits.One,
                DataBits = 8,
                Handshake = Handshake.None
            };

            port.Open();

            _serialPort?.Dispose();
            _serialPort = port;
        }

        public void Send(ISignalSignature signalSignature)
        {
            if (_serialPort == null)
            {
                throw new InvalidOperationException("Serial port not opened.");
            }

            _signalSignatureBuffer[0] = _sendCommand;
            _signalSignatureBuffer[1] = (byte)(signalSignature.Map.Length / 2);

            int messageLength = 2;
            Array.Copy(signalSignature.Map, 0, _signalSignatureBuffer, messageLength, signalSignature.Map.Length);
            messageLength += signalSignature.Map.Length;

            _signalSignatureBuffer[messageLength++] = signalSignature.Duration;
            _signalSignatureBuffer[messageLength++] = (byte)signalSignature.Signal.Length;
            Array.Copy(signalSignature.Signal, 0, _signalSignatureBuffer, messageLength, signalSignature.Signal.Length);
            messageLength += signalSignature.Signal.Length;

            _serialPort.Write(_signalSignatureBuffer, 0, messageLength);
        }

        public void Dispose()
        {
            _serialPort?.Dispose();
            _serialPort = null;
        }
    }
}

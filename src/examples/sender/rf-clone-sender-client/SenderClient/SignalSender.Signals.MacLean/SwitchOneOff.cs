﻿namespace SignalSender.Signals.MacLean
{
    public sealed class SwitchOneOff : MacLeanSignatureBase
    {
        public override byte[] Signal { get; } = new byte[]
        {
            0,0,2,0,0,0,0,0,1,1,1,1,1,0,1,0,1,1,0,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,1,0,1,1,1,1,0,0,0,3
        };
    }
}

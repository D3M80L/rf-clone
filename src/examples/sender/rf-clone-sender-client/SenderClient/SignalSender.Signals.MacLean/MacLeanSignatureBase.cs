﻿namespace SignalSender.Signals.MacLean
{
    public abstract class MacLeanSignatureBase : ISignalSignature
    {
        public byte[] Map { get; } = new byte[]
        {
            5, 5,
            5, 12,
            30, 30,
            5, 90
        };

        public byte Duration { get; } = 105;

        public abstract byte[] Signal { get; }
    }
}

﻿namespace SignalSender.Signals.MacLean
{
    public sealed class SwitchOneOn : MacLeanSignatureBase
    {
        public override byte[] Signal { get; } = new byte[]
        {
            0,0,2,0,0,0,0,0,1,1,1,1,1,0,1,0,1,1,0,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,1,1,0,0,0,0,1,1,1,3
        };
    }
}

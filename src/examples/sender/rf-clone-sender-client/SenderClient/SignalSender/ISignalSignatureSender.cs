﻿using System;

namespace SignalSender
{
    public interface ISignalSignatureSender : IDisposable
    {
        void Send(ISignalSignature signalSignature);
    }
}

﻿namespace SignalSender
{
    public interface ISignalSignature
    {
        byte[] Map { get; }

        byte Duration { get; }

        byte[] Signal { get; }
    }
}

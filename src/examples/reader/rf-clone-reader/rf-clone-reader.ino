/*
 * Original source: http://arduinobasics.blogspot.com.au/2014/06/433-mhz-rf-module-with-arduino-tutorial_27.html
 * 
 * Receiver: XY-MK-5V
 * Description: Read remote RF signal using analogRead
 *
 */

#define DATA_SIZE 1300
#if DATA_SIZE > 1700
  #error 'Arduino memory is limited to max DATA_SIZE=1700'
#endif

#define RF_RECEIVE_PIN A0

#define THRESHOLD_HI 100
#define THRESHOLD_LO 100

#define MAX_SIGNAL_LENGTH 255

byte storedData[DATA_SIZE]; //Create an array to store the data
int dataCounter = 0;        //Variable to measure the length of the signal

void setup()
{
    Serial.begin(9600);

    Serial.println("PRESS KEY ON THE DEVICE.");

    int readValue = analogRead(RF_RECEIVE_PIN);
    while (readValue < THRESHOLD_LO)
    {
      readValue = analogRead(RF_RECEIVE_PIN);
    }

    for (int i = 0; i < DATA_SIZE; ++i)
    {
        // HI signal
        dataCounter = 0;
        while (dataCounter < MAX_SIGNAL_LENGTH && readValue >= THRESHOLD_HI)
        {
            ++dataCounter;
            readValue = analogRead(RF_RECEIVE_PIN);
        }
        storedData[i] = dataCounter;

        // LO signal
        ++i;
        dataCounter = 0;
        while (dataCounter < MAX_SIGNAL_LENGTH && readValue < THRESHOLD_LO)
        {
            ++dataCounter;
            readValue = analogRead(RF_RECEIVE_PIN);
        }
        storedData[i] = dataCounter;
    }

    displayReport();
}

void displayReport()
{
    Serial.println("HIGH,LOW");
    for (int i = 0; i < DATA_SIZE; i += 2)
    {
        Serial.print(storedData[i]);
        Serial.print(",");
        Serial.println(storedData[i + 1]);
    }
}

void loop()
{
    // Do nothing here
}


#define INTERRUPT_PIN 2

volatile byte state = LOW;

void setup()
{
    pinMode(INTERRUPT_PIN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), handleInterrupt, CHANGE);
}

void loop()
{
}

void handleInterrupt()
{
}